FROM python:3-alpine
RUN apk add --no-cache git
RUN pip install --no-cache-dir build twine
WORKDIR /src
ENTRYPOINT git clone "$0" . \
        && python3 -m build --outdir /dist \
        && sha256sum /dist/* \
        && read -p 'Upload these files? [y/N] ' upload \
        && [ "${upload}" = y -o "${upload}" = Y ] \
        && TWINE_USERNAME=__token__ twine upload /dist/*
