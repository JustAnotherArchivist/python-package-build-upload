A tiny Docker image to build a Python package and upload it to PyPI.

Usage:

    docker build -t python-package-build-upload:latest .
    docker run -v mypkg-dist:/dist --rm -ti -e TWINE_PASSWORD=token python-package-build-upload:latest https://example.org/mypkg.git

What it does:

1. Clones the repo.
2. Runs `python3 -m build --outdir /dist` on it. Mounting a volume to `/dist` is optional but recommended if you'd like to keep the original files.
3. Calculates SHA-256 hashes of the dists.
4. Asks you to verify that they should be uploaded to PyPI.
5. Uploads, if confirmed.

The `TWINE_PASSWORD` value must be an API token valid for the project. Username/password authentication is not supported.
